package request_test

import (
	"testing"
	"os"
	"io/ioutil"
	"fmt"
	"strings"
	"regexp"
	"encoding/json"
	"crawler/common/filecache"
)

func TestRewRequest(t *testing.T) {
	filename := "http://www.23us.so/xiaoshuo/25068.html"
	ss := filecache.NewCache(filename, "./")
	path := filecache.GetFileName(filename)
	x, _ := ss.IsExist(path)
	if x == "" {
		ss.MkDir(path)
	}
	reg := regexp.MustCompile(`https://|http://`)
	filename = reg.ReplaceAllString(filename, "")
	filename = strings.Replace(filename, "/", "_", -1)
	filename += ".json"
	fileObj, err := os.Open(filename)
	if err != nil && os.IsNotExist(err) {
		fileObj, _ = os.Create(filename)
	}
	defer fileObj.Close()

	info := make(map[string]interface{})
	info["id"] = 1
	info["title"] = "测试"
	str := "{"
	for key, val := range info {
		str += fmt.Sprintf("\"%v\":\"%v\",", key, val)
	}
	if len(str) > 1 {
		str = str[:len(str)-1]
	}
	str += "}"

	ioutil.WriteFile(filename, []byte(str), 0644)
	b, err := ioutil.ReadFile(filename)
	fmt.Println(string(b))

	post := make(map[string]interface{})
	json.Unmarshal(b, &post)
	// tmp := post.(map[string]interface{})
	fmt.Println(post)
}
