package mysql

import (
	"testing"
	"fmt"
)

func TestConnect_Open(t *testing.T) {
	conn := Open("suyuan", "root", "123456", "127.0.0.1", "3306")
	defer conn.Close()
	data := make(map[string]interface{})
	// where["id"] = 1
	data["title"] = "test"
	data["id"] = 1
	data["name"] = "张三"

	// fields := []string{"id", "title"}
	// limit := []int{1, 20}
	fmt.Println(conn.Into("sy_area", data))
}
