package page

import (
	"io"
	"github.com/henrylee2cn/pholcus/common/goquery"
	"strings"
	"crawler/common/request"
	"time"
	"log"
)

type Page struct {
	// body html
	Title         string
	KeyId         string
	SortField     string
	Tags          string
	Eq            int
	IntoInterface string
	Param         string
	Path          string
	RuleMap       []Rules
	SubPage       *Page
}

type Rules struct {
	Title       string
	MainTag     string
	MainEq      int
	SubTag      string
	SubEq       int
	Replace     string
	RegexpStart string
	RegexpEnd   string
	TagType     int
	FieldName   string
	Process     string
	Prefix      string
	Path        string
}

func NewPage(item map[string]interface{}, rules []Rules, subPage *Page) *Page {
	return &Page{
		Title:         item["title"].(string),
		KeyId:         item["keyId"].(string),
		SortField:     item["sortField"].(string),
		Tags:          item["tags"].(string),
		Eq:            item["eq"].(int),
		Path:          item["path"].(string),
		IntoInterface: item["intoInterface"].(string),
		Param:         item["param"].(string),
		RuleMap:       rules,
		SubPage:       subPage,
	}
}

func timeAfter(d time.Duration) chan int {
	q := make(chan int, 1)

	time.AfterFunc(d, func() {
		q <- 1
	})

	return q
}

func (this Page) MainArea(contentSelection *goquery.Selection, ruleMap []Rules) (map[string]interface{}, string) {
	reData := make(map[string]interface{})
	reUrl := ""
	a := contentSelection.Find("a").Eq(this.Eq - 1)
	if a.Length() == 0 {
		return nil, ""
	}
	href, _ := a.Attr("href")
	if href != "" {
		req, err := request.NewRequest(href, "", "GET", "", nil)
		if err != nil {
			log.Printf("链接：%s 返回空值 \n", href)
			log.Println(err)
			return nil, ""
		}
		reData, reUrl = this.mainAreaInfo(ruleMap, req.GetBody())
	}

	return reData, reUrl
}

func (this Page) mainAreaInfo(rules []Rules, r io.Reader) (map[string]interface{}, string) {
	data := make(map[string]interface{})
	subUrl := ""
	rep, _ := goquery.NewDocumentFromReader(r)
	for _, val := range rules {
		el := rep.Find(val.MainTag).Eq(val.MainEq - 1)
		if val.SubTag != "" {
			el = el.Find(val.SubTag).Eq(val.SubEq - 1)
		}
		if val.TagType == 100 {
			subUrl, _ = el.Attr("href")
		} else {
			// el := getTagArea(r, val.Tag)
			var tagVal string = ""
			switch val.Process {
			case "text":
				tagVal = el.Text()
				if val.Replace != "" {
					tagVal = strings.Replace(tagVal, val.Replace, "", -1)
				}

			case "html":
				htmlStr, _ := el.Html()
				// regexpEnd := formatHtml(val.RegexpEnd)
				tagVal = htmlStr
			case "src":
				imgUrl, _ := el.Attr("src")
				tagVal = imgUrl
			}

			if val.TagType == 2 && len(val.Prefix) > 0 {
				tagVal = val.Prefix + tagVal
			}

			data[val.FieldName] = strings.TrimSpace(tagVal)
			el = nil
		}
	}
	return data, subUrl
}

func intoMain(data map[string]interface{}, url string) {
	if data != nil {

	}
}

func (this Page) GetTagArea(r io.Reader, tag string) (*goquery.Selection) {
	d, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Println("GetTagArea错误:")
		log.Println(err)
	}
	html := d.Find(tag)

	return html
}

func formatHtml(str string) (string) {
	re := strings.Replace(str, "[", "<", -1)
	re = strings.Replace(re, "]", ">", -1)
	return re
}
