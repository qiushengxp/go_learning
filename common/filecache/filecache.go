package filecache

import (
	"os"
	"regexp"
	"strings"
	"io/ioutil"
	"encoding/json"
)

type Cache struct {
	FileName     string   // 文件名
	Path         string   // 所在路径
	CompleteFile string   // 完整文件：路径加文件名
	File         *os.File // 文件对象
}

func NewCache(filename string, path string) (*Cache) {
	cache := new(Cache)
	cache.FileName = GetName(filename) + ".json"
	cache.Path = path

	// 创建目录
	if res, err := IsExist(path); err == nil && res != "dir" {
		MkDir(path)
	}
	if len(filename)>0 {
		cache.CompleteFile = path + "/" + cache.FileName
		// 创建文件
		if res, err := IsExist(cache.CompleteFile); err == nil && res != "file" {
			cache.CreateFile(cache.CompleteFile)
		}
	}
	return cache
}

func (this Cache) CreateFile(filename string) (error) {
	file, err := os.Create(filename)
	this.File = file
	return err
}

func (this Cache) WriteFileFormJson(data map[string]interface{}) {
	b, err := json.Marshal(data)
	if err != nil {
		return
	}
	ioutil.WriteFile(this.CompleteFile, b, 0644)
}

func (this Cache) ReadFileToMap() (map[string]interface{}) {
	b, err := ioutil.ReadFile(this.CompleteFile)
	if err != nil {
		return nil
	}
	data := make(map[string]interface{})
	json.Unmarshal(b, &data)
	return data
}

func (this Cache) Close(){
	this.File.Close()
}

/**
	格式化名称
 */
func GetName(name string) (string) {
	reg := regexp.MustCompile(`https://|http://`)
	name = reg.ReplaceAllString(name, "")
	name = strings.Replace(name, "/", "_", -1)
	return name
}

func MkDir(name string) (error) {
	err := os.Mkdir(name, os.ModePerm)
	return err
}

func IsExist(name string) (string, error) {
	obj, err := os.Stat(name)
	res := ""
	if os.IsNotExist(err) {
		return "", nil
	}
	if err == nil {
		if obj.IsDir() {
			res = "dir"
		} else {
			res = "file"
		}
	}

	return res, nil
}