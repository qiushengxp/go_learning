package util

import (
	"github.com/PuerkitoBio/goquery"
	"log"
	"github.com/axgle/mahonia"
	. "crawler/SiteObject"
)

/*
	获取列表-HTML方式
	例：
	rule := SiteObject.Rules{"#list dd", "a"}
	lists := util.GetListsFormHtml("http://www.biquge.com.tw/14_14055/", rule)
	for i, list := range lists {
		fmt.Print("第", i+1, "章的标题：", list.Title)
		fmt.Println("，链接：", list.Url)
	}
 */
func GetListsFormHtml(url string, rule Rules) ([]List) {
	doc, err := goquery.NewDocument(url)
	if err != nil {
		log.Fatal(err)
	}
	lists := []List{}
	dec := mahonia.NewDecoder("gbk")
	// 循环读取列表项目
	doc.Find(rule.ItemRule).Each(func(i int, cs *goquery.Selection) {
		var list List
		list.Title = dec.ConvertString(cs.Find(rule.UrlRule).Text())
		list.Url, _ = cs.Find(rule.UrlRule).Attr("href")

		lists = append(lists, list)
	})
	return lists
}
