package request

import (
	"net/http"
	"strings"
	"fmt"
	"io"
)

type Request struct {
	Url string

	RespType string
	// GET POST
	Method string
	// POST data
	Postdata string
	// http header
	Header http.Header
	// http cookies
	Cookies []*http.Cookie
	// proxy host   example='localhost:80'
	ProxyHost string

	checkRedirect func(req *http.Request, via []*http.Request) error

	Req *http.Request

	Meta interface{}
}

func NewRequest(urlStr string, respType string, method string, postdata string, meta interface{}) (*Request, error) {
	client := new(http.Client)
	var resp *http.Response
	request := new(Request)
	req, err := http.NewRequest(method, urlStr, strings.NewReader(postdata))
	if (err != nil) {
		return request, err
	}
	resp, err = client.Do(req)
	if (resp != nil && resp.Body != nil) {
		defer resp.Body.Close()
	} else {
		return request, err
	}
	// 打印所有响应头信息
	// for k, v := range resp.Header {
	// 	fmt.Println("响应头信息 :", k, "=", v)
	// }
	request.Url = urlStr
	request.RespType = respType
	request.Method = method
	request.Postdata = postdata
	request.Header = resp.Header
	request.Cookies = resp.Cookies()
	request.Req = req
	// request.checkRedirect = func(req *http.Request, via []*http.Request) {}
	request.Meta = meta
	return request, nil
}

func (this Request) GetBody() (io.ReadCloser) {
	client := &http.Client{}
	rep, err := client.Do(this.Req)
	if err != nil {
		fmt.Println("GetBody错误:")
		fmt.Println(err)
		return nil
	}
	// buf, _ := ioutil.ReadAll(rep.Body)

	return rep.Body
}

func (this Request) GetResponse() (*http.Response) {
	client := &http.Client{}
	rep, _ := client.Do(this.Req)

	return rep
}
