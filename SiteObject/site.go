package SiteObject

/**
	规则
 */
type Rules struct {
	ItemRule string
	UrlRule  string
}

/*
	列表类型
 */
type List struct {
	Title string
	Url   string
}
