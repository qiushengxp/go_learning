package mysql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"fmt"
)

type MysqlConnect struct {
	Db *sql.DB
}

type MysqlModel interface {
	Open()
	Close()
	FindOne(table string, id int64) ([]map[string]string)
}

func Open(dbName string, username string, password string, ip string, port string) (*MysqlConnect) {
	db, err := sql.Open("mysql", username+":"+password+"@tcp("+ip+":"+port+")/"+dbName+"?charset=utf8")
	if err != nil {
		log.Println(err)
	}
	db.SetMaxOpenConns(2000)
	db.SetMaxIdleConns(1000)
	return &MysqlConnect{db}
}

func (this MysqlConnect) Close() {
	this.Db.Close()
}

/*
	根据条件查询单条数据
	where := make(map[string]interface{})
	where["id"] = 1
	where["title"] = "like '%标题%'"
	fields := []string{"id", "title"}
	limit := []int{1, 20}		// 页码（起始：1），页大小，nil则查询所有符合条件记录
	conn.FindOne("表名", fields, where,limit)
 */
func (this MysqlConnect) FindOne(table string, fields []string, condition map[string]interface{}) (map[string]string) {
	list := this.FindList(table, fields, condition, nil)
	if len(list) > 0 {
		return list[0]
	}
	return nil
}

/*
	根据条件查询数据
	fields := []string{"id", "title"}			字段
	where := make(map[string]interface{})		搜索条件
	where["id"] = 1
	where["title"] = "like '%标题%'"
	limit := []int{1,20}
	conn.FindList("表名", fields, where, limit)
 */
func (this MysqlConnect) FindList(table string, fields []string, condition map[string]interface{}, page []int) ([]map[string]string) {
	field := formatField(fields)        // 格式化字段
	where := formatCondition(condition) // 格式化搜索条件
	limit := formatLimit(page)          // 分页
	sql := fmt.Sprintf("select %s from %s %s %s", field, table, where, limit)
	log.Println("执行SQL：", sql)

	rows, err := this.Db.Query(sql)
	if err != nil {
		log.Fatal(err)
	}
	list := returnRows(rows)
	if len(list) > 0 {
		return list
	}
	return nil
}

func (this MysqlConnect) Into(table string, data map[string]interface{}) (int64) {
	fields, values := formatIntoSQL(data)
	if len(fields) <= 0 || len(values) <= 0 || len(table) <= 0 {
		log.Println("数据不完整")
		return -1
	}
	sql := fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)", table, fields, values)
	log.Println("执行SQL：", sql)
	ret, err := this.Db.Exec(sql)
	if err != nil {
		return -1
	}
	// 获取插入ID
	ins_id, _ := ret.LastInsertId()
	return ins_id
}
