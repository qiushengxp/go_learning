package grab

import (
	"crawler/common/mysql"
	"fmt"
	"net/http"
	"crawler/common/page"
	"strconv"
	"strings"
	"crawler/common/request"
	"sync"
	"github.com/henrylee2cn/pholcus/common/goquery"
	"log"
	"time"
	"runtime"
	"io/ioutil"
)

type GrabController struct {
	Conn      *mysql.MysqlConnect
	Writer    http.ResponseWriter
	ChanLimit chan string
}

func (this GrabController) InitGrab(id int64) ([]string, *page.Page) {
	runtime.GOMAXPROCS(runtime.NumCPU()) // 多核
	t1 := time.Now()
	defer this.Conn.Close()
	where := make(map[string]interface{})
	// where["id"] = 1
	where["id"] = id
	ruleInfo := this.Conn.FindOne("phome_collection_sitesrule", nil, where)
	if ruleInfo == nil {
		fmt.Fprintf(this.Writer, "规则不存在！")
	}
	// 创建页面对象
	pages := this.createPage(ruleInfo)
	// 生成链接
	urlMap := strings.Split(ruleInfo["urls"], "{{split}}")
	urls := make([]string, 1)
	var wg sync.WaitGroup

	for _, v := range urlMap {
		for _, val := range getUrlToString(v) {
			wg.Add(1)
			urls = append(urls, val)
			go func(p *page.Page, u string, w *sync.WaitGroup) {
				this.ChanLimit <- u // 启动并发进程累计
				this.RunGrab(u, p, w)
				// i := 0
				// for {
				// 	re := this.RunGrab(u, p, w)
				// 	if re == 1 {
				// 		break
				// 	}
				// 	log.Printf("链接无法读取：%s \n 将在2秒后重试... \n ", u)
				// 	time.Sleep(time.Second * 2)
				// 	if i++; i > 2 {
				// 		log.Printf("链接无法读取：%s \n ", u)
				// 	}
				// }
				w.Done()
				this.runChan(5)
			}(pages, val, &wg)
		}
	}

	wg.Wait()
	// close(this.ChanLimit)
	elapsed := time.Since(t1)
	log.Println("App elapsed: ", elapsed)
	return urls, pages
}

func (this GrabController) runChan(timeout int) {
	select {
	case re := <-this.ChanLimit:
		log.Printf("%s 完成", re)
	case <-time.After(time.Duration(timeout) * time.Second):
		ch := <-this.ChanLimit
		log.Printf("%s 超时", ch)
	}
}

func (this GrabController) RunGrab(urlStr string, pageObj *page.Page, w *sync.WaitGroup) (int) {
	fmt.Fprintf(this.Writer, "开始抓取链接：%s \n", urlStr)
	req, err := request.NewRequest(urlStr, "", "GET", "", nil)
	if err != nil {
		fmt.Fprintf(this.Writer, "链接：%s 返回空值", urlStr)
		return -1
	}
	buf := pageObj.GetTagArea(req.GetBody(), pageObj.Tags)
	datas := make(chan map[string]interface{}, 10)
	subUrl := make(chan string, 10)
	var wg sync.WaitGroup
	buf.Each(func(i int, contentSelection *goquery.Selection) {
		wg.Add(1)
		go func(cs *goquery.Selection, pg *page.Page, sw *sync.WaitGroup, sort int) {
			this.ChanLimit <- strconv.Itoa(sort)
			reData, reUrl := pg.MainArea(cs, pg.RuleMap)
			if len(pg.SortField) > 0 {
				reData[pg.SortField] = sort
			}
			datas <- reData
			subUrl <- reUrl
			sw.Done()
			this.runChan(5)
		}(contentSelection, pageObj, &wg, i)
		// pageObj.MainArea(contentSelection, pageObj.RuleMap)
	})

	wg.Add(1)
	go func(obj *page.Page, sw *sync.WaitGroup) {
		this.ChanLimit <- "主区域读值"
		for {
			select {
			case val, ok := <-datas:
				url := <-subUrl
				if !ok {
					break
				}
				if val != nil {
					log.Printf("抓取小说：%s，链接：%s \n", val["title"], url)
					fmt.Fprintf(this.Writer, "抓取小说：%s，链接：%s\n", val["title"], url)
					//keyId := this.checUnique(obj.Table, obj.Unique, val[obj.Unique].(string))
					//fmt.Println("匹配：", keyId)


					// 暂注释
					// keyId := this.IntoData(obj, val, url)
					// if len(url) > 0 {
					// 	this.ChanLimit <- url
					// 	go this.getSubData(obj.SubPage, url, keyId)
					// }
				}
			}
		}
		sw.Done()
		this.runChan(5)
	}(pageObj, w)
	wg.Wait()
	close(datas)
	close(subUrl)
	fmt.Fprintf(this.Writer, "链接：%s 抓取完成 \n", urlStr)
	return 1
}

func (this GrabController) IntoData(pageObj *page.Page, data map[string]interface{}, subUrl string) (int64) {
	if data == nil {
		return 0
	}
	data["classid"] = pageObj.ClassId
	data["newstempid"] = pageObj.Newstempid

	str := ""
	for i, v := range data {
		switch v.(type) {
		case string:
			str += "&" + i + "=" + v.(string)
		case int:
			str += "&" + i + "=" + strconv.Itoa(v.(int))
		case int64:
			str += "&" + i + "=" + strconv.FormatInt(v.(int64), 10)
		}
	}
	str = str[1:]
	request, _ := http.NewRequest("POST", pageObj.IntoInterface, strings.NewReader(str))   // 请求
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8") // 设置Content-Type
	// request.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36") // 设置User-Agent
	client := new(http.Client)

	response, err := client.Do(request) // 返回
	if err != nil {
		log.Println(err)
	}

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Println(err)
	}
	id, _ := strconv.ParseInt(string(body), 10, 64) // 将返回的 ID 转换成 int64
	response.Body.Close()
	return id
}

func (this GrabController) checUnique(table string, fieldKey string, value string) (int64) {
	var re int64 = -1
	if len(fieldKey) == 0 {
		return re
	}
	where := make(map[string]interface{})
	where[fieldKey] = "='" + value + "'"
	info := this.Conn.FindOne(table, nil, where)
	if info == nil {
		return re
	}
	re, _ = strconv.ParseInt(info["id"], 10, 64)
	return re
}

/**
	子区域读取并入库
 */
func (this GrabController) getSubData(pageObj *page.Page, urlStr string, mainId int64) {
	req, err := request.NewRequest(urlStr, "", "GET", "", nil)
	if err != nil {
		fmt.Fprintf(this.Writer, "子区域抓取失败：链接：%s 返回空值，将在2秒后重试... \n", urlStr)
		log.Printf("子区域抓取失败：链接：%s 返回空值，将在2秒后重试... \n ", urlStr)
		time.Sleep(time.Second * 2)
		req, err = request.NewRequest(urlStr, "", "GET", "", nil)
		if err != nil {
			fmt.Fprintf(this.Writer, "子区域链接：%s 抓取失败 \n ", urlStr)
			log.Printf("子区域链接：%s 抓取失败 \n ", urlStr)
			return
		}
		// x := 0
		// for {
		// 	req, err = request.NewRequest(urlStr, "", "GET", "", nil)
		// 	if err == nil {
		// 		break
		// 	}
		// 	if x++; x > 2 {
		// 		fmt.Fprintf(this.Writer, "子区域链接：%s 抓取失败 \n ", urlStr)
		// 		log.Printf("子区域链接：%s 抓取失败 \n ", urlStr)
		// 		return
		// 	}
		// }
	}
	buf := pageObj.GetTagArea(req.GetBody(), pageObj.Tags)
	// datas := make(chan map[string]interface{}, 10)
	// var wg sync.WaitGroup
	buf.Each(func(i int, contentSelection *goquery.Selection) {
		reData, _ := pageObj.MainArea(contentSelection, pageObj.RuleMap)
		if reData != nil {
			reData[pageObj.KeyId] = mainId
			if len(pageObj.SortField) > 0 {
				reData[pageObj.SortField] = i
			}
			keyId := this.checUnique(pageObj.Table, pageObj.Unique, reData[pageObj.Unique].(string))
			if keyId <= 0 {
				log.Println("插入子区域数据")
				keyId = this.IntoData(pageObj, reData, "")
			} else {
				log.Println("匹配到子区域数据：", keyId)
			}
		}

		// 插入200次则暂停几秒
		if i%200 == 0 {
			time.Sleep(time.Second)
		}
		// wg.Add(1)
		// go func(cs *goquery.Selection, pg *page.Page, sw *sync.WaitGroup, mId int64, sort int) {
		// 	this.ChanLimit <- strconv.Itoa(sort)
		// 	reData, _ := pageObj.MainArea(contentSelection, pageObj.RuleMap)
		// 	if reData != nil {
		// 		reData[pageObj.KeyId] = mId
		// 		if len(pg.SortField) > 0 {
		// 			reData[pageObj.SortField] = sort
		// 		}
		// 		datas <- reData
		// 	}
		// 	sw.Done()
		// 	this.runChan(5)
		// }(contentSelection, pageObj, &wg, mainId, i)
		// 200次则暂停几秒
		// if i%200 == 0 {
		// 	time.Sleep(time.Second * 5)
		// }
	})

	// wg.Add(1)
	// go func(obj *page.Page, sw *sync.WaitGroup) {
	// 	this.ChanLimit <- "子区域读值"
	// 	sleep := 0
	// 	for {
	// 		select {
	// 		case val, ok := <-datas:
	// 			<-this.ChanLimit
	// 			if !ok {
	// 				break
	// 			}
	// 			if val != nil {
	// 				keyId := this.checUnique(obj.Table, obj.Unique, val[obj.Unique].(string))
	// 				fmt.Println("匹配到子区域数据：", keyId)
	// 				keyId = this.IntoData(obj, val, "")
	// 				sleep++
	// 			}
	// 			// 插入200次则暂停几秒
	// 			if sleep%200 == 0 {
	// 				time.Sleep(time.Second * 5)
	// 			}
	// 			// case <-time.After(time.Duration(10) * time.Second):
	// 			// 	ch := <-this.ChanLimit
	// 			// 	log.Printf("%s 超时-子区域", ch)
	// 		}
	// 	}
	// 	sw.Done()
	// 	this.runChan(5)
	// }(pageObj, &wg)
	// wg.Wait()
	this.runChan(5)
	fmt.Fprintf(this.Writer, "链接：%s 抓取结成 \n", urlStr)
}

func (this GrabController) createPage(item map[string]string) (*page.Page) {
	// var getPage ,subPage *page.Page
	 id, _ := strconv.ParseInt(item["id"], 10, 64)
	// classid, _ := strconv.ParseInt(item["main_classid"], 10, 64)
	// sub_classid, _ := strconv.ParseInt(item["sub_classid"], 10, 64)

	info := make(map[string]interface{})
	info["title"] = item["title"]
	info["keyId"] = item["main_key"]
	info["tags"] = item["main_tag"]
	info["eq"], _ = strconv.Atoi(item["main_eq"])
	info["path"] = item["root_path"]
	info["intoInterface"] = item["into_interface"]
	info["param"] = item["main_param"]
	info["sortField"] = item["main_sort"]
	getPage := page.NewPage(info, this.formatRules(id, 1), nil)
	if len(item["subregion_table"]) > 0 {
		info["title"] = item["title"]
		info["keyId"] = item["join_key"]
		info["tags"] = item["sub_tag"]
		info["eq"] = item["sub_eq"]
		info["param"] = item["sub_param"]
		info["sortField"] = item["sub_sort"]
		subPage := page.NewPage(info, this.formatRules(id, 2), nil)
		getPage.SubPage = subPage
	}

	return getPage
}

// 获取页面规则
func (this GrabController) formatRules(rId int64, areaType int) ([]page.Rules) {
	where := make(map[string]interface{})
	where["rid"] = rId
	where["area"] = areaType

	tags := this.Conn.FindList("phome_collection_sitestag", nil, where, nil)
	rules := make([]page.Rules, len(tags))
	for i, item := range tags {
		var rule *page.Rules = new(page.Rules)
		rule.Title = item["title"]
		rule.MainTag = item["main_tag"]
		rule.MainEq, _ = strconv.Atoi(item["main_eq"])
		rule.SubTag = item["sub_tag"]
		rule.SubEq, _ = strconv.Atoi(item["sub_eq"])
		rule.Replace = item["replace"]
		rule.RegexpStart = item["regexp_start"]
		rule.RegexpEnd = item["regexp_end"]
		rule.TagType, _ = strconv.Atoi(item["tag_type"])
		rule.FieldName = item["into_field"]
		rule.Process = item["process"]
		rule.Prefix = item["prefix"]
		rules[i] = *rule
	}
	return rules
}

// 根据规则生成所有URL
func getUrlToString(urlStrin string) ([]string) {
	strStart := strings.Index(urlStrin, "[")
	if (strStart == -1) { // 不存在地址参数则直接返回链接
		return []string{urlStrin}
	}
	strEnd := strings.Index(urlStrin, "]")
	regexp := urlStrin[strStart : strEnd+1]
	strMap := strings.Split(urlStrin[strStart+1:strEnd], ",")

	start, _ := strconv.Atoi(strMap[1])
	end, _ := strconv.Atoi(strMap[2])
	step, _ := strconv.Atoi(strMap[3])
	usrs := make([]string, end)
	var j int = 0
	for i := start; i <= end; i += step {
		usrs[j] = strings.Replace(urlStrin, regexp, strconv.Itoa(i), -1)
		j++
	}
	return usrs
}
