package main

import (
	"net/http"
	"log"
	"fmt"
	"crawler/controller/grab"
	"crawler/common/mysql"
	"strconv"
	"time"
)

// func main() {

// doc, err := goquery.NewDocument("http://www.biquge.com.tw/14_14055/")
// if err != nil {
// 	log.Fatal(err)
// }
// dec := mahonia.NewDecoder("gbk")
// doc.Find("#list dd").Each(func(i int, contentSelection *goquery.Selection) {
// 	title := contentSelection.Find("a").Text()
// 	url, _ := contentSelection.Find("a").Attr("href")
// 	fmt.Print("第", i+1, "章的标题：", dec.ConvertString(title))
// 	fmt.Println("，链接：", url)
// })
// }
type MyHandler map[string]string

func (self MyHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/grab":
		fmt.Fprintf(w, "抓取开始，请等待结束...\n")
		id := req.URL.Query().Get("id")
		conn := mysql.Open("empirecms", "root", "123456", "127.0.0.1", "3306")
		defer conn.Close()
		grabObj := grab.GrabController{conn, w,make(chan string,50)}
		id64, _ := strconv.ParseInt(id, 10, 64)
		grabObj.InitGrab(id64)
		// fmt.Println(urls)
		// for _, v := range urls {
		// 	fmt.Println(v)
		// 	grabObj.RunGrab(v, pages)
		// }
		time.Sleep(time.Second * 1)
	default:
		w.WriteHeader(http.StatusNotFound) // 404
		fmt.Fprintf(w, "no such page: %s\n", req.URL)
	}
}

func main() {
	handler := MyHandler{}
	log.Fatal(http.ListenAndServe("localhost:8000", handler))
	fmt.Println("listen:8000")
}
