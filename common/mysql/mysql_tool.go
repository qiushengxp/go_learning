package mysql

import (
	"database/sql"
	"strconv"
	"strings"
)

func returnRows(rows *sql.Rows) ([]map[string]string) {
	columns, _ := rows.Columns()
	values := make([]sql.RawBytes, len(columns))
	scans := make([]interface{}, len(columns))

	for i := range values {
		scans[i] = &values[i]
	}

	var result []map[string]string
	for rows.Next() {
		_ = rows.Scan(scans...)
		each := make(map[string]string)
		for i, col := range values {
			each[columns[i]] = string(col) // 转换格式
		}

		result = append(result, each)

	}
	return result
}

func formatCondition(where map[string]interface{}) string {
	if where == nil {
		return ""
	}
	str := "where 1=1"
	for i, v := range where {
		switch v.(type) {
		case string:
			str += " AND " + i + " " + v.(string)
		case int:
			str += " AND " + i + "=" + strconv.Itoa(v.(int))
		case int64:
			str += " AND " + i + "=" + strconv.FormatInt(v.(int64), 10)
		}
	}
	return str
}

func formatField(fields []string) string {
	if fields == nil {
		return "*"
	}
	str := strings.Join(fields, ",")

	return str
}

func formatLimit(limit []int) string {
	if limit == nil {
		return ""
	}
	str := "limit"
	if len(limit) > 1 {
		start := (limit[0] - 1) * limit[1]
		end := start + limit[1]
		str += " " + strconv.Itoa(start) + "," + strconv.Itoa(end)
	} else {
		str += " " + strconv.Itoa(limit[0])
	}
	return str
}

func formatIntoSQL(data map[string]interface{}) (string, string) {
	fields := ""
	values := ""
	if len(data) == 0 {
		return "", ""
	}
	for key, val := range data {
		fields += "," + key
		switch val.(type) {
		case string:
			values += ",'" + val.(string) + "'"
		case int:
			values += "," + strconv.Itoa(val.(int))
		case int64:
			values += "," + strconv.FormatInt(val.(int64), 10)
		}

	}

	return fields[1:], values[1:]
}
